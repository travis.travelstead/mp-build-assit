# mp-build-assit

Micropython project build assistant

## Environment

### Install packages
- git
- ARM compiler
- DFU untils
- Lib USB for DFU

os_build_prep.py can install packages, run as sudo
```shell
python3 sudo os_build_pre.py
```

or individual

```shell
sudo apt-get install update
sudo apt-get install upgrade
sudo apt-get install gcc-arm-none-eabi
sudo apt-get install dfu-util
sudi apt-get install libusb-1.0-0-dev

```

**In some cases "gcc" and "make" need to also be installed.**

### Initial configure of Micropython

#### init submodules

```shell
git submodule update --init
```

#### make cross compiler
Cross compiler needs to be built to allow for for including micropython code into the DFU

```shell
micropython/mpy-cross/make
```

#### create mboot for board
Due to uniqueness in the new bootloader system, each board needs its own mboot DFU.

```shell
micropython/ports/stm32/mboot/make BOARD=name_of_board MBOOT=1
```

### Code repo
The code repo needs to a "deploy_config.py" file which holds the files that are part of the full DFU, development DFU, and also what is synced during development.

EXAMPLE:
```python
files_deploy = (
    'boot.py',
    'main.py',
    'app.py',
)

files_develop = (
    'boot.py',
    'main.py',
)

files_sync = (
    ('app.py', 'std'),
)
```

### Build board script.

- discovery available micropython and repos
- set mp
- set repe
- list boards
- set board
- config
- clean
- move dev or move deploy

## Notes

```
make BOARD=board_name USE_MBOOT=1
```

``` shell
dfu-util -a 0 -d 0483:df11 -D firmware.dfu
```


     ┌─────────────────────┐  ┌─────────────────────┐  ┌─────────────────────┐
     │Micropython Repo     │  │                     │  │                     │
     │- boards             │  │Code Repo            │  │                     │
     │- mboot              │  │- deploy_config.py   │  │     DFU builds      │
     │- to_freeze          │  │- asset_dest         │  │                     │
     │- make script        │  │- to_freeze          │  │                     │
     │                     │  │                     │  │                     │
     └─────────────────────┘  └─────────────────────┘  └─────────────────────┘
                ▲                        │                        ▲
                │                        │                        │
                │                        │                        │
                │                        │                        │
                │                        │                        │
                │                        │                        │
                └───────────────┐        │       ┌────────────────┘
                                │        │       │
                                │        │       │
                                │        │       │
                                │        │       │
                                │        │       │
                                │        │       │
                                ▼        ▼       │
                                ┌────────────────┐
                                │                │
                                │ Working directory  │
                                │                │
                                └────────────────┘