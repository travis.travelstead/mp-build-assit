import sys
sys.dont_write_bytecode = True

import subprocess
import os

package_list = [
    'gcc-arm-none-eabi',
    'dfu-util',
    'libusb-1.0-0-dev',
]


class OSconfig:

    def __init__(self):

        self.error_count = 0

    def package_prep(self, verbose=False):
        print('Package prep')
        print('Package update and upgrade')

        return_update = 0
        return_upgrade = 0

        if verbose:
            return_update = subprocess.call(['apt-get', 'update'])
        else:
            return_update = subprocess.call(['apt-get', 'update'], stdout=open(os.devnull, 'wb'), stderr=subprocess.STDOUT)

        if return_update != 0:
            print('FAILED - ERROR: {}'.format(return_update))
            self.error_count += 1
        else:
            print('GOOD - Update')

        if verbose:
            return_upgrade = subprocess.call(['apt-get', 'upgrade'])
        else:
            return_upgrade = subprocess.call(['apt-get', 'upgrade'], stdout=open(os.devnull, 'wb'), stderr=subprocess.STDOUT)

        if return_upgrade != 0:
            print('FAILED - Upgrade, ERROR: {}'.format(return_upgrade))
            self.error_count += 1
        else:
            print('GOOD - Upgrade')

    def install_package(self, package_name, verbose=False):
        print()
        print('Install package: {}'.format(package_name))
        return_code = 0

        if verbose:
            return_code = subprocess.call(['apt-get', 'install', package_name, '-y'])
        else:
            return_code = subprocess.call(['apt-get', 'install', package_name, '-y'], stdout=open(os.devnull, 'wb'), stderr=subprocess.STDOUT)

        if return_code != 0:
            print('FAILED - Package {} , ERROR: {}'.format(package_name, return_code))
            self.error_count += 1
        else:
            print('GOOD - Package {}'.format(package_name))

    def prep_directory(self):

        print('Create firmware collection point')
        if os.path.isdir('firmware') is False:
            os.mkdir('firmware')

    def prep_micropython(self):

        # # git submodule update --init
        # return_submodule = subprocess.call(['git', 'submodule', 'update', '--init'])
        # print(return_submodule)

        # return_cross = subprocess.call(['git', 'submodule', 'update', '--init'])

        print('Micropython specfic setup')
        print('From micropyhton root folder, "git submodule update --init" ')
        print('From micropython/mpycross folder, "make" ')

# def pipInstall(packageName, verbose=False):
# 	print('')
# 	print('---- Package: ' + packageName)
# 	theStartTime = time.time()

# 	print(subprocess.call(['pip3', 'install', packageName], stdout=open(os.devnull,'wb'), stderr=subprocess.STDOUT))

# 	print('Package timer: ' + str(time.time() - theStartTime))
# 	print('')

# 	return 0


if __name__ == "__main__":
    print()
    print('Prep operating system for Micropython')
    print()
    osconfig = OSconfig()

    osconfig.package_prep()
    for pack in package_list:
        osconfig.install_package(pack)

    osconfig.prep_directory()

    osconfig.prep_micropython()
