import sys
sys.dont_write_bytecode = True

import os
from cmd import Cmd
import subprocess
import shutil

from BoardBuild import BuildBoard


class BuildPrompt(Cmd):

    def __init__(self):
        Cmd.__init__(self)

        self.VERSION = 'v0.1'
        self.prompt = '(BUILD)>> '
        self.intro = 'BUILD tool ' + self.VERSION

        self.build_board = BuildBoard()

        self.MICROPYTHON_DEF = 'micropython'
        self.REPO_DEF = 'deploy_config.py'

        self.micropython_list = []
        self.repo_list = []
        self.board_list = []

        self.selection_mp = None
        self.selection_repo = None
        self.selection_board = None

        self.firmware_name = None

    def do_discovery(self, args):
        ''' Discovery repos '''
        print()
        dir_contents = os.listdir()
        for item in dir_contents:
            if os.path.isdir(item):
                if item.startswith(self.MICROPYTHON_DEF):
                    self.micropython_list.append(item)
                    print('Found Micropython: {}'.format(item))
                else:
                    sub_contents = os.listdir(item)
                    if self.REPO_DEF in sub_contents:
                        self.repo_list.append(item)
                        print('Found Repo: {}'.format(item))
                    else:
                        print('Unknown: {}'.format(item))

        self.list_all()

    def do_config(self, args):
        ''' Set configurations '''
        print()
        if self.selection_mp is not None and self.selection_repo is not None:
            self.build_board.config(self.selection_board, self.selection_mp, self.selection_repo)
        else:
            print('Cannot configure, Micropython and/or Code Repo not set')
            self.list_all()

    def do_list(self, args):
        ''' List Micropython and Repo options '''
        print()
        if args == 'mp' or args == 'micropython':
            self.list_mp()
        elif args == 'repo':
            self.list_repo()
        elif args == 'board' or 'boards':
            self.list_board()
        elif args == '':
            self.list_all()
        else:
            print('LIST - Unknown argrument')

    def list_all(self):
        print('-- Micropython')
        self.list_mp()
        print('-- Repo')
        self.list_repo()
        print('-- Board')
        self.list_board()

    def list_mp(self):
        if len(self.micropython_list) > 0:
            for count, mp in enumerate(self.micropython_list):
                print('[{}] - {}'.format(count, mp))
        else:
            print('No discovered Micropython')

    def list_repo(self):
        if len(self.repo_list) > 0:
            for count, repo in enumerate(self.repo_list):
                print('[{}] - {}'.format(count, repo))
        else:
            print('No discovered repos')

    def list_board(self):
        if self.selection_mp is not None:
            dir_contents = os.listdir('{}/ports/stm32/boards/'.format(self.selection_mp))
            for item in dir_contents:
                if os.path.isdir('{}/ports/stm32/boards/{}'.format(self.selection_mp, item)):
                    self.board_list.append(item)
            for count, board in enumerate(self.board_list):
                print('[{}] - {}'.format(count, board))
        else:
            print('Micropython not set')

    def do_set(self, args):
        ''' Set micropython, repo, or board '''
        print()

        arg_type = None
        arg_num = None

        try:
            the_args = args.split()
            arg_type = the_args[0]
            try:
                arg_num = int(the_args[1])
            except Exception:
                arg_num = None
        except Exception:
            print('Single Argument')
            arg_type = args

        if arg_type == 'mp' or arg_type == 'micropython':
            self.set_mp(arg_num)
        elif arg_type == 'repo':
            self.set_repo(arg_num)
        elif arg_type == 'board':
            self.set_board(arg_num)
        else:
            print('SET - Unknown argrument')

    def set_mp(self, selection):
        if len(self.micropython_list) > 0:
            if selection is None:
                selection = 0
                print('No argument provided, default to [0]')

            try:
                self.selection_mp = self.micropython_list[selection]
                print('Micropython now set to: {}'.format(self.selection_mp))
            except IndexError:
                self.selection_mp = None
                print('Argument not valid')
            except ValueError:
                self.selection_mp = None
                print('Argument not valid')
        else:
            print('No discovered Micropython repos to select, run discovery cmd')

    def set_repo(self, selection):
        if len(self.repo_list) > 0:
            if selection is None:
                selection = 0
                print('No argument provided, default to [0]')

            try:
                self.selection_repo = self.repo_list[selection]
                print('Micropython now set to: {}'.format(self.selection_repo))
            except IndexError:
                self.selection_repo = None
                print('Argument not valid')
            except ValueError:
                self.selection_repo = None
                print('Argument not valid')
        else:
            print('No discovered repos to select, run discovery cmd')

    def set_board(self, selection):
        if len(self.board_list) > 0:
            if selection is None:
                selection = 0
                print('No argument provided, default to [0]')

            try:
                self.selection_board = self.board_list[selection]
                print('Board now set to: {}'.format(self.selection_board))
            except IndexError:
                self.selection_board = None
                print('Argument not valid')
            except ValueError:
                self.selection_board = None
                print('Argument not valid')
        else:
            print('No discovered repos to select, run discovery cmd')

    def do_clean(self, args):
        ''' Clean up to_freeze folder in build '''
        print()
        if self.build_board.is_config:
            self.build_board.clean_to_freeze()
        else:
            print('Configuration not set')

    def do_move(self, args):
        ''' Move code files into to freeze build folder '''
        print()
        if self.build_board.is_config:
            if args == 'dev':
                self.build_board.move_files_to_freeze(self.build_board.file_list_develop)
            elif args == '' or args == 'dep':
                self.build_board.move_files_to_freeze(self.build_board.file_list_deploy)
            else:
                print('MOVE - Argument not valid')
        else:
            print('MOVE - Configuration not set')

    def do_make(self, args):
        ''' Run make for mboot or board '''
        # subprocess.call(['make',], cwd='micropython-1_11-3/ports/stm32/', shell=True)

        return_code = False
        print('Board firmware build')
        if self.build_board.is_config and self.selection_board is not None:
            if args == '':
                return_code = subprocess.call(self.build_board.BOARD_MAKE, shell=True, cwd=self.build_board.BUILD_PORT_PATH)
                print('Return Code: {}'.format(return_code))
            elif args == 'mboot':
                # make BOARD=PYBV10 USE_MBOOT=1 clean all deploy
                return_code = subprocess.call(self.build_board.BOARD_MAKE, shell=True, cwd=self.build_board.BUILD_PORT_PATH)
                print('Return Code: {}'.format(return_code))
            else:
                print('MAKE - Argument not valid')
        else:
            print('MAKE - Not configured and/or board selected')

        if return_code == 0:
            self.firmware_name = 'firmware/dfu_{}_{}_{}.dfu'.format(self.selection_repo, self.selection_board, self.selection_mp)
            if os.path.isdir('firmware') is False:
                os.mkdir('firmware')
                print('Created firmware folder')
            shutil.copy('{}/firmware.dfu'.format(self.build_board.BUILD_PORT_COMPILE_PATH), self.firmware_name)
            print('Firmware build, {}'.format(self.firmware_name))
        else:
            print('MAKE - Return code shows errors')

    def do_selected(self, args):
        ''' Show currently selected configurations '''
        print()
        print('Micropython: {}'.format(self.selection_mp))
        print('Repository: {}'.format(self.selection_repo))
        print('Board: {}'.format(self.selection_board))
        print('Is configured: {}'.format(self.build_board.is_config))

    def do_dfu(self, args):
        ''' Run dfu update '''
        if args == '':
            # dfu-util -a 0 -d 0483:df11 -D firmware.dfu
            subprocess.call(self.build_board.BOARD_MAKE, shell=True, cwd=self.build_board.BUILD_PORT_PATH)
            # dfu firmware
            pass
        elif args == 'mboot':
            # dfu mboot file
            pass

    def do_load(self, args):
        ''' Load configuration for faster delpoyment '''
        pass

    def do_show(self, args):
        ''' Show selected '''
        print()
        print('Selected Micropython: {}'.format(self.selection_mp))
        print('Selected Repo: {}'.format(self.selection_repo))

    def do_exit(self, args):
        ''' Exit prompt '''
        print('Exiting deploy prompt')
        print()
        return True


if __name__ == "__main__":

    prompt = BuildPrompt()
    prompt.cmdloop()
