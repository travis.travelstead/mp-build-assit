

import importlib
import shutil
import os


class BuildBoard:

    def __init__(self):

        self.BUILD_TYPE_DEPLOY = 'deploy'
        self.BUILD_TYPE_DEVELOP = 'develope'

        self.code_to_freeze_folder = False
        self.code_asset_folder = False

        self.file_list_deploy = None
        self.file_list_develop = None

        self.BOARD_NAME = None
        self.REPO_BUILD = None
        self.REPO_CODE = None

        self.CODE_FREEZE_DIR = None
        self.CODE_ASSET_DIR = None
        self.CODE_DEPLOY_CONFIG = None

        self.BUILD_PORT_PATH = None
        self.BUILD_BOARD_PATH = None
        self.BUILD_FREEZE_PATH = None

        self.BOARD_MAKE = None

        self.is_config = False

    def config(self, board_name, repo_build, repo_code):

        self.REPO_BUILD = repo_build
        self.REPO_CODE = repo_code

        self.CODE_FREEZE_DIR = '{}/to_freeze'.format(self.REPO_CODE)
        self.CODE_ASSET_DIR = '{}/asset_dest'.format(self.REPO_CODE)
        self.CODE_DEPLOY_CONFIG = '{}/deploy_config.py'.format(self.REPO_CODE)

        deploy_config = importlib.machinery.SourceFileLoader('deploy_config', self.CODE_DEPLOY_CONFIG).load_module()
        print('Imported: {}'.format(self.CODE_DEPLOY_CONFIG))

        try:
            self.file_list_deploy = deploy_config.files_deploy
        except Exception:
            print('Could not find deploy file list')
        try:
            self.file_list_develop = deploy_config.files_develop
        except Exception:
            print('Could not find development file list')

        self.BOARD_NAME = board_name

        self.BUILD_PORT_PATH = '{}/ports/stm32'.format(self.REPO_BUILD)
        self.BUILD_BOOT_PATH = '{}/mboot'.format(self.BUILD_PORT_PATH)
        self.BUILD_PORT_COMPILE_PATH = '{}/build-{}'.format(self.BUILD_PORT_PATH, self.BOARD_NAME)
        self.BUILD_BOARD_PATH = '{}/boards/{}'.format(self.BUILD_PORT_PATH, self.BOARD_NAME)
        self.BUILD_FREEZE_PATH = '{}/to_freeze'.format(self.BUILD_BOARD_PATH)

        # make BOARD=board_name FROZEN_MPY_DIR="boards/board_name/to_freeze" USE_MBOOT=1
        self.BOARD_MAKE = 'make BOARD={} FROZEN_MPY_DIR="boards/{}/to_freeze" USE_MBOOT=1'.format(self.BOARD_NAME, self.BOARD_NAME)

        print('Settings configured')
        self.is_config = True
        self.check_locations()

    def check_locations(self):
        ''' Debugging feedback on which files and pathes are found '''
        print('Check file locations')
        print()
        print('Check: {}  , exist: {}'.format(self.BUILD_PORT_PATH, os.path.exists(self.BUILD_PORT_PATH)))
        print('Check: {}  , exist: {}'.format(self.BUILD_BOARD_PATH, os.path.exists(self.BUILD_BOARD_PATH)))
        print('Check: {}  , exist: {}'.format(self.BUILD_FREEZE_PATH, os.path.exists(self.BUILD_FREEZE_PATH)))
        print('Check: {}  , exist: {}'.format(self.CODE_FREEZE_DIR, os.path.exists(self.CODE_FREEZE_DIR)))
        print('Check: {}  , exist: {}'.format(self.CODE_ASSET_DIR, os.path.exists(self.CODE_ASSET_DIR)))
        print('Check: {}  , exist: {}'.format(self.CODE_DEPLOY_CONFIG, os.path.exists(self.CODE_DEPLOY_CONFIG)))

        if os.path.exists(self.CODE_FREEZE_DIR):
            self.code_to_freeze_folder = True

        if os.path.exists(self.CODE_ASSET_DIR):
            self.code_asset_folder = True

    def clean_to_freeze(self):
        ''' Clear existing to_freeze folder and create new empty one'''
        print()

        if self.is_config:
            if os.path.isdir(self.BUILD_FREEZE_PATH):
                try:
                    shutil.rmtree(self.BUILD_FREEZE_PATH)
                    print('Removed: {}'.format(self.BUILD_FREEZE_PATH))
                except Exception as e:
                    print('Failed to remove folder: {}'.format(self.BUILD_FREEZE_PATH))
                    print(e)

            try:
                os.mkdir(self.BUILD_FREEZE_PATH)
                print('Created: {}'.format(self.BUILD_FREEZE_PATH))
            except Exception as e:
                print('Failed to create folder: {}'.format(self.BUILD_FREEZE_PATH))
                print(e)
        else:
            print('Configuration is not set')

    def move_files_to_freeze(self, file_list):

        if self.is_config and file_list is not None:

            if self.code_asset_folder:
                for asset_file in os.listdir(self.CODE_ASSET_DIR):
                    if asset_file.endswith('.py'):
                        path_src = '{}/{}'.format(self.CODE_ASSET_DIR, asset_file)
                        path_dst = '{}/{}'.format(self.BUILD_FREEZE_PATH, asset_file)
                        if os.path.exists(path_src):
                            shutil.copy(path_src, path_dst)
                            print('Moving: {}'.format(asset_file))
                    else:
                        print('Asset file is not python source file.  {}'.format(asset_file))
            else:
                print('Asset folder not used')

            if self.code_to_freeze_folder:
                for freeze_file in os.listdir(self.CODE_FREEZE_DIR):
                    if freeze_file.endswith('.py'):
                        path_src = '{}/{}'.format(self.CODE_FREEZE_DIR, freeze_file)
                        path_dst = '{}/{}'.format(self.BUILD_FREEZE_PATH, freeze_file)
                        if os.path.exists(path_src):
                            shutil.copy(path_src, path_dst)
                            print('Moving: {}'.format(freeze_file))
                    else:
                        print('Freeze file is not python source file.  {}'.format(freeze_file))
            else:
                print('Freeze folder not used')

            for source_file in file_list:
                if source_file.endswith('.py'):
                    path_src = '{}/{}'.format(self.REPO_CODE, source_file)
                    path_dst = '{}/{}'.format(self.BUILD_FREEZE_PATH, source_file)
                    if os.path.exists(path_src):
                        shutil.copy(path_src, path_dst)
                        print('Moving: {}'.format(source_file))
                else:
                    print('File is not python source file.  {}'.format(source_file))

        else:
            print('Configuration is not set, or file list is not found')

